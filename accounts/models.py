from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField("Avatar", upload_to="profile-images")
    bio = models.TextField("Bio", max_length=150)

    def __str__(self):
        return self.user.username


class Follower(models.Model):
    name_of_follower = models.ForeignKey(
        User,
        related_name="name_of_followers",
        on_delete=models.CASCADE,
        null=True,
    )

    artist_followed = models.ForeignKey(
        User,
        related_name="artist_followeds",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta:
        unique_together = ("name_of_follower", "artist_followed")

    def __str__(self):
        return self.name_of_follower.username
