from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordResetView
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from arts.models import Art
from accounts.models import Profile, Follower
from accounts.forms import ProfileForm, FollowerForm


# Create your views here.
def user_login(request):
    form = LoginForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        remember_me = form.cleaned_data["remember_me"]
        user = authenticate(
            request,
            username=username,
            password=password,
        )
        if not remember_me:
            request.session.set_expiry(0)
            request.session.modified = True
        if user is not None:
            login(request, user)
            return redirect("home")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            email = form.cleaned_data["email"]
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                if not last_name:
                    last_name = ""
                user = User.objects.create_user(
                    username=username,
                    email=email,
                    first_name=first_name,
                    last_name=last_name,
                    password=password,
                )

                login(request, user)

                return redirect("home")
            else:
                form.add_error("password", "The passwords do not match!")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)


class ResetPasswordView(SuccessMessageMixin, PasswordResetView):
    template_name = "accounts/reset.html"
    email_template_name = "accounts/reset_email.html"
    subject_template_name = "accounts/reset_subject.txt"
    success_message = (
        "Please check your email for the instructions to reset your password. "
        "If you do not receiive an email,"
        "please re-enter the correct email address "
        "or check your spam folder. Thank you."
    )
    success_url = reverse_lazy("login")


@login_required(login_url="login")
def profile(request, id):
    artist = User.objects.get(id=id)
    arts = Art.objects.filter(artist=artist).order_by("-date")
    follower = Follower.objects.filter(artist_followed=artist)
    following = Follower.objects.filter(name_of_follower=artist)
    followed = Follower.objects.filter(
        name_of_follower=request.user, artist_followed=artist
    ).exists()

    context = {
        "art_list": arts,
        "artist": artist,
        "followers": follower,
        "following": following,
        "followed": followed,
    }

    if request.method == "POST" and "unfollow" in request.POST:
        follow = Follower.objects.filter(
            name_of_follower=request.user, artist_followed=artist
        )
        follow.delete()
        return redirect("user_profile", id=artist.id)

    if request.method == "POST" and "unfollow" not in request.POST:
        form = FollowerForm(request.POST)
        context = {
            "art_list": arts,
            "artist": artist,
            "followers": follower,
            "following": following,
            "followed": followed,
            "form": form,
        }
        if form.is_valid():
            follow = form.save(commit=False)
            follow.artist_followed = artist
            follow.name_of_follower = request.user
            follow.save()
            return redirect("user_profile", id=artist.id)
        else:
            form = FollowerForm()

    return render(request, "accounts/profile.html", context)


@login_required(login_url="login")
def create_profile(request):
    if Profile.objects.filter(user=request.user).exists():
        return redirect("edit_profile", id=request.user.profile.id)
    if request.method == "POST":
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            profile = form.save(commit=False)
            profile.user = request.user
            profile.save()
            return redirect("user_profile", id=request.user.id)
    else:
        form = ProfileForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create_profile.html", context)


@login_required(login_url="login")
def edit_profile(request, id):
    profile = Profile.objects.get(id=id)
    if request.method == "POST":
        form = ProfileForm(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            profile = form.save()
            return redirect("user_profile", id=id)
    else:
        form = ProfileForm(instance=profile)
    context = {
        "form": form,
    }
    return render(request, "accounts/edit_profile.html", context)


@login_required(login_url="login")
def follower_list(request, id):
    artist = User.objects.get(id=id)
    follower = Follower.objects.filter(artist_followed=artist)
    following = Follower.objects.filter(name_of_follower=artist)

    if request.method == "POST" and request.user == artist:
        follower_id = request.POST.get("follower_id")
        if follower_id:
            follow = Follower.objects.get(id=follower_id)
            follow.delete()
        followee_id = request.POST.get("followee_id")
        if followee_id:
            follow = Follower.objects.get(id=followee_id)
            follow.delete()
        return redirect("show_follow", id=artist.id)

    context = {
        "artist": artist,
        "follower": follower,
        "followee": following,
    }
    return render(request, "accounts/follow.html", context)
