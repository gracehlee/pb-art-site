from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.forms import ModelForm
from accounts.models import Profile, Follower


username_validator = RegexValidator(
    r"^[a-zA-Z0-9][a-zA-Z0-9._]*[a-zA-Z0-9]$",
    "Invalid username - Alphanumerical characters and (_) and (.) allowed;"
    + " first and last must be alphanumerical.",
)


name_validator = RegexValidator(
    r"^[a-zA-Z]+( [a-zA-Z]+)*$", "Invalid Name - letters only."
)


password_validator = RegexValidator(
    r"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%%^&*()_+={};:,.<>?~-]).*$",
    "Invalid password - Needs at least one uppercase,"
    + " one number, one lowercase, and one special symbol:"
    + r"[!@#$%%^&*()_+={};:,.<>?~-]",
)


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "class": "form-control",
            }
        ),
    )
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "class": "form-control",
                "data-toggle": "password",
                "id": "password",
                "name": "password",
            }
        ),
    )
    remember_me = forms.BooleanField(required=False)

    class Meta:
        model = User
        fields = ["username", "password", "remember_me"]


class SignUpForm(forms.Form):
    username = forms.CharField(
        min_length=3,
        max_length=30,
        required=True,
        validators=[
            username_validator,
        ],
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "class": "form-control",
            }
        ),
    )

    def clean_username(self):
        username = self.cleaned_data.get("username")
        if username:
            username = username.lower()
            return username

    email = forms.EmailField(
        required=True,
        widget=forms.TextInput(
            attrs={
                "placeholder": "Email",
                "class": "form-control",
            }
        ),
    )
    first_name = forms.CharField(
        max_length=30,
        required=True,
        validators=[
            name_validator,
        ],
        widget=forms.TextInput(
            attrs={
                "placeholder": "First Name",
                "class": "form-control",
            }
        ),
    )
    last_name = forms.CharField(
        max_length=30,
        validators=[
            name_validator,
        ],
        required=False,
        widget=forms.TextInput(
            attrs={
                "placeholder": "Last Name",
                "class": "form-control",
            }
        ),
    )
    password = forms.CharField(
        min_length=8,
        max_length=300,
        validators=[
            password_validator,
        ],
        required=True,
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "class": "form-control",
                "data-toggle": "password",
                "id": "password",
            }
        ),
    )
    password_confirmation = forms.CharField(
        min_length=8,
        max_length=300,
        required=True,
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Confirm Password",
                "class": "form-control",
                "data-toggle": "password",
                "id": "password",
            }
        ),
    )


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = [
            "avatar",
            "bio",
        ]


class FollowerForm(ModelForm):
    class Meta:
        model = Follower
        fields = []
