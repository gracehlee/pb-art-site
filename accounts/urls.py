from django.urls import path
from accounts.views import (
    user_login,
    user_logout,
    signup,
    profile,
    create_profile,
    edit_profile,
    follower_list,
)

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
    path("profile/<int:id>/", profile, name="user_profile"),
    path("create/", create_profile, name="create_profile"),
    path("edit/<int:id>/", edit_profile, name="edit_profile"),
    path("followers/<int:id>/", follower_list, name="show_follow"),
]
