from django.contrib import admin
from accounts.models import Profile, Follower


# Register your models here.
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "avatar",
        "bio",
        "id",
    ]


@admin.register(Follower)
class FollowerAdmin(admin.ModelAdmin):
    list_display = [
        "name_of_follower",
        "artist_followed",
        "id",
    ]
