# Generated by Django 5.0.2 on 2024-02-13 00:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="profile",
            name="avatar",
            field=models.ImageField(
                default="accounts/static/images/sidebar_icon.png",
                upload_to="images/",
                verbose_name="Avatar",
            ),
        ),
    ]
