## Name: PB Art Site
![icon](accounts/static/images/site.png)

Demo Video: https://youtu.be/i6nX5Bb1AIU

## Description
This is a project dedicated to build out different functions of Django; designed to look like a personal art site. The site view was loosely based off Instagram, and functionality was built with sites such as ArtFol, DeviantArt, and Twitter (X) in mind.
- It's designed to allow the user to create an art portfolio
  by uploading images directly to the site.
- It allows users to explore other users' works in the "Explore" tab.
- The "Explore" tab has a search bar, allowing users to search by any combination of tags,
  artist names, and artwork titles.
- It has a follower-system where users can follow or unfollow other users,
  as well as see the list of followers and followees.
- The home page displays artworks by the user and anyone the user follows.
- Background functions include password validation and "Forgot Password" email functionality.

## Specs
- primarily Python
- Django framework
- pure HTML & CSS
- some minor JS

## Installation
This project's secret key has been changed and hidden using .gitignore and with a .env file. As a result, anyone forking and cloning this repo MUST create their own .env file in their pb-art-site directory with the following contents:

SECRET_KEY=[your secret key here (no brackets)]

The secret key can be set to any arbitrary value. After setting the secret key, restart the server if you have one running.

```
python -m venv .venv
.venv\Scripts\Activate.ps1
pip install -r requirements.txt
python manage.py runserver

```
