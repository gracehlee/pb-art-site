from django.contrib import admin
from arts.models import Art, Tag


# Register your models here.
@admin.register(Art)
class ArtAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "file",
        "description",
        "date",
        "timestamp",
        "artist",
        "display_tags",
        "private",
        "id",
    ]

    def display_tags(self, obj):
        return ", ".join([tag.name for tag in obj.tags.all()])

    display_tags.short_description = "Tags"


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "creator",
        "id",
    ]
