from django.db import models
from django.contrib.auth.models import User
import datetime


# Create your models here.
class Art(models.Model):
    name = models.CharField("Name", max_length=200, default="Untitled")
    file = models.ImageField(
        "Picture",
        default=("static/images/sidebar_icon.png"),
        upload_to="images/",
    )
    description = models.TextField("Description", max_length=1000, blank=True)
    date = models.DateField("Date", default=datetime.date.today)
    timestamp = models.DateTimeField("Timestamp", default=datetime.datetime.now)
    artist = models.ForeignKey(
        User,
        related_name="arts",
        on_delete=models.CASCADE,
        null=True,
    )
    tags = models.ManyToManyField("Tag", related_name="tags", blank=True)
    private = models.BooleanField("Private", default=False)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField("Name", max_length=200, unique=True)
    creator = models.ForeignKey(
        User,
        related_name="tags",
        on_delete=models.CASCADE,
        null=True,
    )

    def save(self, *args, **kwargs):
        self.name = self.name.lower()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name
