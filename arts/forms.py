from django.forms import ModelForm
from arts.models import Art, Tag


class ArtForm(ModelForm):
    class Meta:
        model = Art
        fields = [
            "name",
            "file",
            "description",
            "date",
            "tags",
            "private",
        ]


class TagForm(ModelForm):
    class Meta:
        model = Tag
        fields = [
            "name",
        ]
