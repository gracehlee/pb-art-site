from django.urls import path
from arts.views import (
    art_list,
    show_art,
    explore_art,
    upload_art,
    edit_art,
    create_tag,
    submitted,
    tag_list,
    edit_tag,
    submitted_edit,
    delete_art,
    delete_tag,
)


urlpatterns = [
    path("", art_list, name="art_list"),
    path("<int:id>/", show_art, name="show_art"),
    path("explore/", explore_art, name="explore"),
    path("upload/", upload_art, name="upload"),
    path("edit/<int:id>/", edit_art, name="edit"),
    path("create/", create_tag, name="create_tag"),
    path("submitted/", submitted, name="submitted"),
    path("submitted/edit/", submitted_edit, name="submitted_edit"),
    path("tag-list/", tag_list, name="tag_list"),
    path("edit-tag/<int:id>/", edit_tag, name="edit_tag"),
    path("delete/<int:id>/", delete_art, name="delete_art"),
    path("delete/tag/<int:id>/", delete_tag, name="delete_tag"),
]
