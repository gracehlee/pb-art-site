from django.shortcuts import render, redirect
from arts.models import Art, Tag
from accounts.models import Follower
from django.contrib.auth.decorators import login_required
from arts.forms import ArtForm, TagForm
from django.db.models import Q


# Create your views here.
@login_required(login_url="login")
def art_list(request):
    follower = Follower.objects.filter(name_of_follower=request.user)
    followed_artists = [follow.artist_followed for follow in follower]
    arts = Art.objects.filter(
        Q(artist=request.user) | Q(artist__in=followed_artists)
    ).order_by("-timestamp")
    context = {
        "art_list": arts,
    }
    return render(request, "arts/list.html", context)


@login_required(login_url="login")
def show_art(request, id):
    art = Art.objects.get(id=id)
    context = {
        "art_detail": art,
    }
    return render(request, "arts/detail.html", context)


@login_required(login_url="login")
def explore_art(request):
    arts = Art.objects.all().order_by("-timestamp")
    query_tag = request.GET.get("q_tag")
    query_artist = request.GET.get("q_artist")
    query_title = request.GET.get("q_title")
    if query_tag:
        query_tag = query_tag.lower()
        arts = arts.filter(tags__name__icontains=query_tag)
    if query_artist:
        arts = arts.filter(artist__username__iexact=query_artist)
    if query_title:
        arts = arts.filter(name__icontains=query_title)
    context = {
        "art_list": arts,
    }
    return render(request, "arts/explore.html", context)


@login_required(login_url="login")
def upload_art(request):
    if request.method == "POST":
        form = ArtForm(request.POST, request.FILES)
        if form.is_valid():
            art = form.save(commit=False)
            art.artist = request.user
            art.save()
            form.save_m2m()
            return redirect("show_art", id=art.id)
    else:
        form = ArtForm()
    context = {
        "form": form,
    }
    return render(request, "arts/upload.html", context)


@login_required(login_url="login")
def edit_art(request, id):
    art = Art.objects.get(id=id)
    if request.method == "POST":
        form = ArtForm(request.POST, request.FILES, instance=art)
        if form.is_valid():
            art = form.save()
            return redirect("show_art", id=art.id)
    else:
        form = ArtForm(instance=art)
    context = {
        "form": form,
    }
    return render(request, "arts/edit.html", context)


@login_required(login_url="login")
def create_tag(request):
    if request.method == "GET":
        request.session["previous_page"] = request.META.get("HTTP_REFERER")
    if request.method == "POST":
        form = TagForm(request.POST)
        if form.is_valid():
            tag = form.save(commit=False)
            tag.creator = request.user
            tag.save()
            return redirect("submitted")
    else:
        form = TagForm()
    context = {
        "form": form,
    }
    return render(request, "arts/create_tag.html", context)


@login_required(login_url="login")
def submitted(request):
    return render(request, "arts/submitted.html")


@login_required(login_url="login")
def tag_list(request):
    tags = Tag.objects.filter(creator=request.user)
    context = {
        "tag_list": tags,
    }
    return render(request, "arts/tags.html", context)


@login_required(login_url="login")
def edit_tag(request, id):
    tag = Tag.objects.get(id=id)
    if request.method == "POST":
        form = TagForm(request.POST, instance=tag)
        if form.is_valid():
            tag = form.save()
            return redirect("submitted_edit")
    else:
        form = TagForm(instance=tag)
    context = {
        "tag": tag,
        "form": form,
    }
    return render(request, "arts/edit_tag.html", context)


@login_required(login_url="login")
def submitted_edit(request):
    return render(request, "arts/submitted_edit.html")


@login_required(login_url="login")
def delete_art(request, id):
    art = Art.objects.get(id=id)
    if request.method == "POST":
        art.delete()
        return redirect("home")
    return render(request, "arts/delete.html")


@login_required(login_url="login")
def delete_tag(request, id):
    tag = Tag.objects.get(id=id)
    if request.method == "POST":
        tag.delete()
        return redirect("home")
    return render(request, "arts/delete.html")


@login_required(login_url="login")
def profile(request):
    return render(request, "accounts/profile.html")
